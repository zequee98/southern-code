const config = {
  baseURL: process.env.REACT_APP_API_URL,
  nasaKey: process.env.REACT_APP_NASA_KEY,
}

export { config }
