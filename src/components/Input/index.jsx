import { useState, useEffect } from 'react'

function Input({ onInputChange, value, maxSol }) {
  const [isError, setIsError] = useState(false)

  useEffect(() => {
    if (value > maxSol) {
      setIsError(true)
    } else {
      setIsError(false)
    }
  }, [value, maxSol, isError])

  return (
    <div style={{ marginBottom: 24 }}>
      <p>Sol</p>
      <input name="sol" onChange={onInputChange} type="text" value={value} />
      {isError && (
        <p style={{ margin: 0, marginTop: 4, color: 'red', fontSize: 11 }}>
          The max number is {maxSol}
        </p>
      )}
    </div>
  )
}

export { Input }
