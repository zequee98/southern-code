function Pagination({ photosLength, page, handleNextPage, handlePreviousPage, isFetching }) {
  return (
    <div style={{ display: 'flex', marginBottom: 16, marginTop: 16 }}>
      <button disabled={page < 2} onClick={handlePreviousPage} type="button">
        Previous
      </button>
      <button disabled={isFetching || photosLength !== 25} onClick={handleNextPage} type="button">
        Next
      </button>
    </div>
  )
}

export { Pagination }
