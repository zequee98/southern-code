export const FILTERS_INITIAL_STATE = {
  roverName: 'curiosity',
  cameraName: '',
  sol: '1000',
}

export const ROVER_NAMES_OPTIONS = [
  {
    name: 'Curiosity',
    value: 'curiosity',
  },
  {
    name: 'Opportunity',
    value: 'opportunity',
  },
  {
    name: 'Spirit',
    value: 'spirit',
  },
]

export const ROVER_CAMERAS_OPTIONS = [
  {
    name: 'All',
    value: '',
  },
  {
    name: 'FHAZ',
    value: 'FHAZ',
  },
  {
    name: 'RHAZ',
    value: 'RHAZ',
  },
  {
    name: 'MAST',
    value: 'MAST',
  },
  {
    name: 'CHEMCAM',
    value: 'CHEMCAM',
  },
  {
    name: 'MAHLI',
    value: 'MAHLI',
  },
  {
    name: 'MARDI',
    value: 'MARDI',
  },
  {
    name: 'NAVCAM',
    value: 'NAVCAM',
  },
  {
    name: 'PANCAM',
    value: 'PANCAM',
  },
  {
    name: 'MINITES',
    value: 'MINITES',
  },
]
