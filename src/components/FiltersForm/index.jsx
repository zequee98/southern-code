import { useState, useCallback } from 'react'

import { marsRoverHooks } from '@slices'

import { Input } from '../Input'
import { Select } from '../Select'
import { FILTERS_INITIAL_STATE, ROVER_NAMES_OPTIONS, ROVER_CAMERAS_OPTIONS } from './utils'

function FiltersForm({
  handleCleanFilters,
  setIsDisableGetRoverByName,
  handleResetPagination,
  handleSearch,
}) {
  const [filters, setFilters] = useState(FILTERS_INITIAL_STATE)
  const { data: manifest = {} } = marsRoverHooks.useGetRoverManifestByNameQuery({
    roverName: filters.roverName,
  })

  const handleInputChange = (event) => {
    const { value, name } = event.target

    setFilters((prevFilters) => ({
      ...prevFilters,
      [name]: value,
    }))
  }

  const handleReset = useCallback(() => {
    setFilters(FILTERS_INITIAL_STATE)
    setIsDisableGetRoverByName(true)
    handleCleanFilters()
    handleResetPagination()
  }, [handleCleanFilters, setIsDisableGetRoverByName, handleResetPagination])

  return (
    <div style={{ border: '1px solid', padding: 8 }}>
      <h3>Searcher</h3>
      <Select
        label="Rover to fetch:"
        name="roverName"
        onChange={handleInputChange}
        options={ROVER_NAMES_OPTIONS}
        value={filters.roverName}
      />
      <Select
        label="Filter the rover photos by camera:"
        name="cameraName"
        onChange={handleInputChange}
        options={ROVER_CAMERAS_OPTIONS}
        value={filters.cameraName}
      />
      <Input maxSol={manifest.max_sol} onInputChange={handleInputChange} value={filters.sol} />

      <button onClick={() => handleSearch(filters)} type="button">
        Search
      </button>

      <button onClick={handleReset} type="button">
        Reset
      </button>
    </div>
  )
}

export { FiltersForm }
