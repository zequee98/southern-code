function QueryDescription({ isUninitialized, roverName, cameraName, sol, handleSave }) {
  if (isUninitialized) {
    return <h3>Latest Photos</h3>
  }

  return (
    <div style={{ border: '1px solid', padding: 8, marginTop: 16, marginBottom: 16 }}>
      <h3>Filters:</h3>
      <p>Rover: {roverName}</p>
      {cameraName && <p>Camera: {cameraName}</p>}
      <p>Sol date: {sol}</p>
      <button onClick={handleSave} type="button">
        Save
      </button>
    </div>
  )
}

export { QueryDescription }
