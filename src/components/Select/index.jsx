function Select({ value, onChange, options, name, label }) {
  return (
    <>
      <p>{label}</p>
      <select name={name} onChange={onChange} value={value}>
        {options.map((item) => (
          <option key={item.name} value={item.value}>
            {item.name}
          </option>
        ))}
      </select>
    </>
  )
}

export { Select }
