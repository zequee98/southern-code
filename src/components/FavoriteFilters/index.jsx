function FavoriteFilters({ favoriteFilters, handleSearch }) {
  return (
    <div style={{ border: '1px solid', padding: 8 }}>
      <h3>Favorite Filters</h3>
      {favoriteFilters.map((favoriteFilter) => (
        <div
          key={`${favoriteFilter.roverName}${favoriteFilter.cameraName}${favoriteFilter.sol}`}
          style={{ border: '1px solid', padding: 8 }}
        >
          <p>Rover: {favoriteFilter.roverName}</p>
          {favoriteFilter.cameraName && <p>Camera: {favoriteFilter.cameraName}</p>}
          <p>Sol date: {favoriteFilter.sol}</p>
          <button onClick={() => handleSearch(favoriteFilter)} type="button">
            Search
          </button>
        </div>
      ))}
    </div>
  )
}

export { FavoriteFilters }
