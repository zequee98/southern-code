function PhotosList({ photos }) {
  return photos.map((photo) => (
    <img key={photo.id} alt={photo.img_src} height="250" src={photo.img_src} width="250" />
  ))
}

export { PhotosList }
