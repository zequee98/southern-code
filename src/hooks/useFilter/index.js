import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { filtersActions } from '@slices'

const useFilter = ({ handleResetPagination, setIsDisableGetRoverByName }) => {
  const { filters } = useSelector((state) => state)
  const dispatch = useDispatch()

  const handleSetFilters = useCallback(
    (newFilters) => {
      dispatch(filtersActions.setFilters(newFilters))
    },
    [dispatch],
  )

  const handleCleanFilters = useCallback(() => {
    dispatch(filtersActions.cleanFilters())
  }, [dispatch])

  const handleSearch = useCallback(
    (favoriteFilter) => {
      handleSetFilters(favoriteFilter || filters)
      setIsDisableGetRoverByName(false)
      handleResetPagination()
    },
    [handleSetFilters, filters, setIsDisableGetRoverByName, handleResetPagination],
  )

  return {
    filters,
    handleSetFilters,
    handleCleanFilters,
    handleSearch,
  }
}

export { useFilter }
