import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { favoriteFiltersActions } from '@slices'

const useFavoriteFilters = () => {
  const { favoriteFilters } = useSelector((state) => state)
  const dispatch = useDispatch()

  const handleAddFavoriteFilter = useCallback(
    (filter) => {
      dispatch(favoriteFiltersActions.addFavorite(filter))
    },
    [dispatch],
  )

  return {
    favoriteFilters: Object.values(favoriteFilters),
    handleAddFavoriteFilter,
  }
}

export { useFavoriteFilters }
