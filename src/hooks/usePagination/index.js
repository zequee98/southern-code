import { useState, useCallback } from 'react'

const usePagination = () => {
  const [page, setPage] = useState(1)

  const handleNextPage = useCallback(() => {
    setPage((prevPage) => prevPage + 1)
  }, [])

  const handlePreviousPage = useCallback(() => {
    setPage((prevPage) => prevPage - 1)
  }, [])

  const handleResetPagination = useCallback(() => {
    setPage(1)
  }, [])

  return {
    page,
    handleNextPage,
    handlePreviousPage,
    handleResetPagination,
  }
}

export { usePagination }
