import { marsRoverHooks } from '@slices'

const useMarsRover = ({ filters, isDisableGetRoverByName, page }) => {
  /**
   * Get latest photos by Rover name
   */
  const { data: latestPhotos = [], isFetching: isLatestPhotosFetching } =
    marsRoverHooks.useGetRoverLatestPhotosByNameQuery({
      roverName: filters.roverName,
      page,
    })

  /**
   * Get Mars Rover photos by params
   */
  const {
    data: photos = [],
    isFetching: isPhotoFetching,
    isUninitialized: isPhotoUninitialized,
  } = marsRoverHooks.useGetRoverByNameQuery(
    {
      roverName: filters.roverName,
      cameraName: filters.cameraName,
      sol: filters.sol,
      page,
    },
    { skip: isDisableGetRoverByName },
  )

  return {
    photos,
    isPhotoFetching,
    isPhotoUninitialized,
    latestPhotos,
    isLatestPhotosFetching,
  }
}

export { useMarsRover }
