import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import { mainPages } from './Main'

function Routes() {
  return (
    <Router>
      <Switch>
        {mainPages.map((page) => (
          <Route key={page.key} {...page} />
        ))}
      </Switch>
    </Router>
  )
}

export { Routes }
