import { Home } from './Home'
import { Favorites } from './Favorites'

const mainPages = [
  {
    path: '/',
    component: Home,
    exact: true,
    key: 'Home',
  },
  {
    path: '/favorites',
    component: Favorites,
    exact: true,
    key: 'Favorites',
  },
]

export { mainPages }
