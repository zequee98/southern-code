import { useState, useCallback } from 'react'
import { Link } from 'react-router-dom'

import { useFilter, useFavoriteFilters, usePagination, useMarsRover } from '@hooks'
import { Pagination, FiltersForm, PhotosList, QueryDescription } from '@components'

function Home() {
  const [isDisableGetRoverByName, setIsDisableGetRoverByName] = useState(true)
  const { page, handleNextPage, handlePreviousPage, handleResetPagination } = usePagination()
  const { handleAddFavoriteFilter } = useFavoriteFilters()
  const { filters, handleSearch, handleCleanFilters } = useFilter({
    handleResetPagination,
    setIsDisableGetRoverByName,
  })
  const { photos, isPhotoFetching, isPhotoUninitialized, latestPhotos, isLatestPhotosFetching } =
    useMarsRover({ filters, isDisableGetRoverByName, page })

  const handleSave = useCallback(() => {
    handleAddFavoriteFilter(filters)
  }, [handleAddFavoriteFilter, filters])

  return (
    <div>
      <Link to="/favorites">Favorites</Link>
      <FiltersForm
        filters={filters}
        handleCleanFilters={handleCleanFilters}
        handleResetPagination={handleResetPagination}
        handleSearch={handleSearch}
        setIsDisableGetRoverByName={setIsDisableGetRoverByName}
      />

      <QueryDescription
        cameraName={filters.cameraName}
        handleSave={handleSave}
        isUninitialized={isPhotoUninitialized}
        roverName={filters.roverName}
        sol={filters.sol}
      />

      <Pagination
        handleNextPage={handleNextPage}
        handlePreviousPage={handlePreviousPage}
        isFetching={isPhotoFetching || isLatestPhotosFetching}
        page={page}
        photosLength={isPhotoUninitialized ? latestPhotos.length : photos.length}
      />
      {(isPhotoFetching || isLatestPhotosFetching) && <p>Loading...</p>}

      <PhotosList photos={isPhotoUninitialized ? latestPhotos : photos} />
    </div>
  )
}

export { Home }
