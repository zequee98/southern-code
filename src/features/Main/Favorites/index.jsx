import { useState } from 'react'

import { useFilter, useFavoriteFilters, usePagination, useMarsRover } from '@hooks'
import { Pagination, PhotosList, FavoriteFilters } from '@components'

function Favorites() {
  const [isDisableGetRoverByName, setIsDisableGetRoverByName] = useState(true)
  const { page, handleNextPage, handlePreviousPage, handleResetPagination } = usePagination()
  const { favoriteFilters } = useFavoriteFilters()
  const { filters, handleSearch } = useFilter({
    handleResetPagination,
    setIsDisableGetRoverByName,
  })
  const { isPhotoFetching, photos } = useMarsRover({ filters, isDisableGetRoverByName, page })

  return (
    <div>
      <FavoriteFilters favoriteFilters={favoriteFilters} handleSearch={handleSearch} />

      <Pagination
        handleNextPage={handleNextPage}
        handlePreviousPage={handlePreviousPage}
        isFetching={isPhotoFetching}
        page={page}
        photosLength={photos.length}
      />
      {isPhotoFetching && <p>Loading...</p>}

      <PhotosList photos={photos} />
    </div>
  )
}

export { Favorites }
