import axios from 'axios'

import { config } from '@config'

const api = axios.create({
  baseURL: config.baseURL,
  params: {
    api_key: config.nasaKey,
  },
})

const axiosBaseQuery =
  () =>
  async ({ url, method, data }) => {
    try {
      const result = await api({ url, method, data })
      return { data: result.data }
    } catch (axiosError) {
      const err = axiosError
      return {
        error: { status: err.response?.status, data: err.response?.data },
      }
    }
  }

export { axiosBaseQuery }
