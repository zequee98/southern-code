import { createApi } from '@reduxjs/toolkit/query/react'

import { axiosBaseQuery } from '@api'

const REDUCER_PATH = 'marsRover'
const MARS_PHOTO_BASE_API = 'mars-photos/api/v1'

const marsRoverSlice = createApi({
  reducerPath: REDUCER_PATH,
  baseQuery: axiosBaseQuery(),
  endpoints: (builder) => ({
    /**
     * Get latest photos
     * @param {string} roverName
     * @param {string} page
     */
    getRoverLatestPhotosByName: builder.query({
      query: ({ roverName, page }) => ({
        url: `${MARS_PHOTO_BASE_API}/rovers/${roverName}/latest_photos?page=${page}`,
        method: 'get',
      }),
      transformResponse: (response) => response.latest_photos,
    }),

    /**
     * Get Mars Rover photos
     * @param {string} roverName
     * @param {string} cameraName
     * @param {string} page
     * @param {string} sol
     */
    getRoverByName: builder.query({
      query: ({ roverName, cameraName, page, sol }) => {
        const cameraParams = `${cameraName ? `&camera=${cameraName}` : ''}`

        return {
          url: `${MARS_PHOTO_BASE_API}/rovers/${roverName}/photos?sol=${sol}&page=${page}${cameraParams}`,
          method: 'get',
        }
      },
      transformResponse: (response) => response.photos,
    }),

    /**
     * Get Rover Manifest
     * @param {string} roverName
     */
    getRoverManifestByName: builder.query({
      query: ({ roverName = 'Curiosity' }) => ({
        url: `${MARS_PHOTO_BASE_API}/manifests/${roverName}`,
        method: 'get',
      }),
      transformResponse: (response) => response.photo_manifest,
    }),
  }),
})

const marsRoverHooks = {
  useGetRoverByNameQuery: marsRoverSlice.useGetRoverByNameQuery,
  useGetRoverManifestByNameQuery: marsRoverSlice.useGetRoverManifestByNameQuery,
  useGetRoverLatestPhotosByNameQuery: marsRoverSlice.useGetRoverLatestPhotosByNameQuery,
}

export { marsRoverHooks, marsRoverSlice }
