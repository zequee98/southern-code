import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {}
const NAME = 'favoriteFilters'

const favoriteFiltersSlice = createSlice({
  name: NAME,
  initialState: INITIAL_STATE,
  reducers: {
    addFavorite: (state, action) => {
      const filter = JSON.stringify(action.payload)
      return { ...state, [filter]: filter }
    },
  },
})

export const { reducer: favoriteFiltersReducer, actions: favoriteFiltersActions } =
  favoriteFiltersSlice
