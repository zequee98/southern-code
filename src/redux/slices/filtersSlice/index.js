import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = { roverName: 'curiosity', cameraName: 'MAST', sol: '1000' }
const NAME = 'filter'

const filtersSlice = createSlice({
  name: NAME,
  initialState: INITIAL_STATE,
  reducers: {
    setFilters: (state, action) => {
      const filters = action.payload
      return { ...state, ...filters }
    },
    cleanFilters: () => INITIAL_STATE,
  },
})

export const { reducer: filtersReducer, actions: filtersActions } = filtersSlice
