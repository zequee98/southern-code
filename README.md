# Challenge


## Setup

- `git clone`
- `yarn install`
- replace `.env.template` by `.env`, then add your `NASA_KEY`

```bash
REACT_APP_NASA_KEY = 'Replace for your nasa key'
```

- `yarn start`
