const path = require('path')

const { override, addWebpackAlias, disableEsLint } = require('customize-cra')

const alias = {
  '@features': path.resolve(__dirname, 'src/features'),
  '@components': path.resolve(__dirname, 'src/components'),
  '@hooks': path.resolve(__dirname, 'src/hooks'),
  '@config': path.resolve(__dirname, 'src/config'),
  '@api': path.resolve(__dirname, 'src/api'),
  '@store': path.resolve(__dirname, 'src/redux/store'),
  '@slices': path.resolve(__dirname, 'src/redux/slices'),
}

module.exports = override(addWebpackAlias(alias), disableEsLint())
